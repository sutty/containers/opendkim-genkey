#!/bin/sh
set -e

cd /etc/opendkim

test -f ${SELECTOR:-dkim}.private && exit 0

/usr/bin/opendkim-genkey \
  --bits=${BITS:-2048} \
  --domain=${SUTTY:-sutty.nl} \
  --restrict \
  --selector=${SELECTOR:-dkim} \
  --subdomains \
  --verbose

chown -R opendkim:opendkim .
