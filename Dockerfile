FROM alpine:latest
MAINTAINER "f <f@sutty.nl>"

ENV SUTTY sutty.nl
ENV BITS 2048
ENV SELECTOR dkim

RUN apk add --no-cache opendkim-utils
COPY ./opendkim-genkey.sh /usr/local/bin/opendkim-genkey
RUN chmod 755 /usr/local/bin/opendkim-genkey

ENTRYPOINT /usr/local/bin/opendkim-genkey
